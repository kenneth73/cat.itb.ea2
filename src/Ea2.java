import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class Ea2 {

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int opcio;

        System.out.println("\nMenu Principal");
        System.out.println("1. Ex1: Consultar les dades d'un empleat del fitxer" +
                "\n2. Ex2: Insertar dades en el fitxer" +
                "\n3. Ex3: Modificar dades en el fitxer" +
                "\n4. Ex4: Esborrar dades en el fitxer" +
                "\n5. Ex5: Mostrar els identificadors dels empleats eliminats");

        System.out.println("Introduiex el numero de exercici que vols executar, del 1 al 5");
        opcio = llegirIntMinMax(0,5);
        switch (opcio) {
            case 0:
                System.out.println("Fi del programa");
                break;
            case 1:
                exercici1();
                break;
            case 2:
                exercici2();
                break;
            case 3:
                exercici3();
                break;
            case 4:
                exercici4();
                break;
            case 5:
                exercici5();
                break;
            default:
                System.out.println("ERROR, OPCIO NO VÀLIDA!");
                break;
        }
    }

    //1.- Consultar les dades d'un empleat del fitxer "AleatorioEmple.dat". El mètode ha de rebre un identificador
    //d'empleat que li passem per paràmetre o bé l'introduïm per teclat. Si l'empleat existeix, es visualitzaran les seves
    //dades, si no existeix es visualitzarà un missatge que ho indiqui.
    public static void exercici1() throws IOException {

        Scanner sc = new Scanner(System.in);
        File fichero = new File("AleatorioEmple.dat");
        RandomAccessFile file = new RandomAccessFile(fichero, "r");

        Double salario;
        char apellido[] = new char[10];

        System.out.println("Introdueix l'ID del empleat a buscar : ");
        int id = llegirIntTeclat();
        int posicion = (id - 1) * 36;

        if (posicion < file.length()) {
            file.seek(posicion);
            id = file.readInt();

            for (int i = 0; i < apellido.length; i++) {
                apellido[i] = file.readChar();
            }

            String apellidos = new String(apellido);
            int dep = file.readInt();
            salario = file.readDouble();

            System.out.println("ID : " + id + "\nCognom : " + apellidos.trim()
                    + "\nDepartament : " + dep + "\nSalari : " + salario);

        } else {
            System.out.println("El empleado amb id "+id+" no existeix!");
        }
        sc.close();
        file.close();
        System.out.println("");
    }

    //2.- Insertar dades en el fitxer "AleatorioEmple.dat". El mètode ha de rebre 4 paràmetres: identificador
    //d'empleat, cognom, departament i salari, que li passem per paràmetre o bé introduïm per teclat. Abans d'insertar
    //es comprovarà si l'identificador existeix, si existeix s'ha de visualitzar un missatge que ho indiqui, si no existeix
    //s'ha d'insertar.
    public static void exercici2() throws IOException {
        Scanner sc = new Scanner(System.in);
        File fichero = new File("AleatorioEmple.dat");
        RandomAccessFile file = new RandomAccessFile(fichero, "rw");

        int id, dep, posicion;
        Double salario;
        char apellido[] = new char[10];
        StringBuffer buffer;

        System.out.println("Introdueix l'ID : ");
        id = sc.nextInt();
        posicion = (id - 1) * 36;

        if (posicion >= file.length()) {
            file.seek(posicion);
            file.writeInt(id);

            System.out.println("Introdueix el cognom:");
            String apellidoStr = sc.next();
            buffer = new StringBuffer(apellidoStr);
            buffer.setLength(10);
            file.writeChars(buffer.toString());
            System.out.println("Introdueix el departament:");
            dep = sc.nextInt();
            file.writeInt(dep);
            System.out.println("Introdueix el salari:");
            salario = sc.nextDouble();
            file.writeDouble(salario);
        } else {
            System.out.println("Error! Ya existeix un empleat amb l'ID " + id);
        }
        sc.close();
        file.close();
    }

    //3.- Modificar dades en el fitxer "AleatorioEmple.dat". El mètode rep pel teclat o li passem com a paràmetres
    //un identificador d'empleat i un import. S'ha de realitzar la modificació del salari. La modificació consistirà en
    //sumar al salari de l'empleat l'import introduït. El mètode ha de visualitzar el cognom, el salari antic i el nou. Si
    //l'identificador no existeix, es visualitzarà un missatge que ho indiqui.
    public static void exercici3() throws IOException {
        Scanner sc = new Scanner(System.in);
        File fichero = new File("AleatorioEmple.dat");
        RandomAccessFile file = new RandomAccessFile(fichero, "rw");

        long posicio;

        char apellido[] = new char[10], aux;

        System.out.println("Introdueix l'ID de l'empleat que vols editar: ");
        int id = llegirIntTeclat();
        posicio = (id - 1) * 36;

        if (posicio < fichero.length()) {
            System.out.println("Introdueix l'increment de sou per l'empleat amb id " +id +" :");
            Double incrementSou = sc.nextDouble();
            file.seek(posicio);

            for (int i = 0; i < apellido.length; i++) {
                aux = file.readChar();
                apellido[i] = aux;
            }

            String cognom = new String(apellido);
            posicio = posicio + 4 + 20 + 4; // ID + COGNOM + DEPARTAMENT
            file.seek(posicio);
            Double salari = file.readDouble();
            Double salariAntic = salari;
            Double salariNou = salari + incrementSou;
            file.seek(posicio);
            file.writeDouble(salariNou);
            System.out.println("Cognom : " + cognom.trim() + "\nSalari Antic : " + salariAntic
                    + "\nSalari NoU : " + salariNou);
        } else {
            System.out.println("No existeix l'empleat amb id " + id);
        }
        sc.close();
        file.close();
    }

    //4.- Esborrar dades en el fitxer "AleatorioEmple.dat". Crea un mètode que rebi pel teclat o li passem com a
    //paràmetre un identificador d'empleat i l'esborri. Serà un esborrat lògic marcant el registre amb la següent
    //informació: l'identificador serà "-1", el cognom serà l'identificador que s'elimina, i el departament i el salari
    //seran 0.
    public static void exercici4() throws IOException {
        Scanner sc = new Scanner(System.in);
        File fichero = new File("AleatorioEmple.dat");
        RandomAccessFile file = new RandomAccessFile(fichero, "rw");

        System.out.println("Introdueix l'ID de l'empleat a eliminar : ");
        int id = llegirIntTeclat();
        int posicio = (id - 1) * 36;
        file.seek(posicio);

        if (posicio < fichero.length()) {
            StringBuilder idDelete = new StringBuilder(String.valueOf(id));
            idDelete.setLength(10);
            file.seek(posicio);
            file.writeInt(-1);
            file.writeChars(idDelete.toString());
            file.writeInt(0);
            file.writeDouble(0);
            System.out.println("L'empleat amb ID " + id + " s'ha eliminat correctament.");
        } else {
            System.out.println("L'empleat amb ID "+ id +" no existe!");
        }
        sc.close();
        file.close();
    }

    //5.- Crea un mètode que mostri els identificadors dels empleats eliminats.
    public static void exercici5() throws IOException {
        File fichero = new File("AleatorioEmple.dat");
        RandomAccessFile file = new RandomAccessFile(fichero, "rw");
        StringBuilder registresEliminats = new StringBuilder();

        int posicio = 0;
        char[] apellido = new char[10];
        while (posicio < file.length()) {
            file.seek(posicio);
            if (file.readInt() == -1) {
                registresEliminats.append("ID:");
                for (int i = 0; i < 10; i++)
                    apellido[i] = file.readChar();
                registresEliminats.append(new String(apellido).trim());
                registresEliminats.append(", ");
            }
            posicio += 36;
        }
        file.close();
        System.out.println("Usuaris eliminats: " + registresEliminats.toString());
    }

    public static int llegirIntTeclat() {
        Scanner lector = new Scanner(System.in);
        int enterLlegit = 0;
        boolean llegit = false;
        while (!llegit) {
            llegit = lector.hasNextInt();
            if (llegit) {
                enterLlegit = lector.nextInt();
            } else {
                System.out.println("ERROR DE TIPUS.");
                lector.next();
            }
        }
        lector.nextLine();
        return enterLlegit;
    }

    public static int llegirIntMinMax (int min, int max){
        int enterLlegit ;
        enterLlegit = llegirIntTeclat();
        while (enterLlegit < min || enterLlegit > max){
            System.out.println("ERROR DE RANG. MÍNIM: "+ min + ", MÀXIM: " + max);
            enterLlegit = llegirIntTeclat();
        }
        return enterLlegit;
    }
}