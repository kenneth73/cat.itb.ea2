import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class Ex1 {
    /*
    1.- Consultar les dades d'un empleat del fitxer "AleatorioEmple.dat". El mètode ha de rebre un identificador
    d'empleat que li passem per paràmetre o bé l'introduïm per teclat. Si l'empleat existeix, es visualitzaran les seves
    dades, si no existeix es visualitzarà un missatge que ho indiqui.
    *
    * */

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        File fichero = new File("AleatorioEmple.dat");
        //declara el fichero de acceso aleatorio
        RandomAccessFile file = new RandomAccessFile(fichero, "r");
        //
        int  id, dep, posicion;
        Double salario;
        char apellido[] = new char[10], aux;
        System.out.println("Introduce el id del empleado: ");

        int registro = llegirIntMinMax(1,100); //recoger id de empleado a consultar

        posicion = (registro -1 ) * 36;
        if(posicion >= file.length() )
            System.out.printf("ID: %d, NO EXISTE EMPLEADO...",registro);
        else{
            System.out.println("Posicio del cursor: "+file.getFilePointer());
            file.seek(posicion); //nos posicionamos
            System.out.println("Posicio del cursor: "+file.getFilePointer());
            id=file.readInt(); // obtengo id de empleado
            System.out.println("Posicio del cursor: "+file.getFilePointer());
            for (int i = 0; i < apellido.length; i++) {
                aux = file.readChar();//recorro uno a uno los caracteres del apellido
                apellido[i] = aux;    //los voy guardando en el array
            }
            System.out.println("Posicio del cursor: "+file.getFilePointer());
            String apellidoS= new String(apellido);//convierto a String el array
            dep=file.readInt();//obtengo dep
            System.out.println("Posicio del cursor: "+file.getFilePointer());
            salario=file.readDouble();  //obtengo salario
            System.out.println("Posicio del cursor: "+file.getFilePointer());
            System.out.println("ID: " + registro + ", Apellido: "+
                    apellidoS.trim() +
                    ", Departamento: "+dep + ", Salario: " + salario);
        }
        System.out.println("Posicio del cursor: "+file.getFilePointer());
        file.close();  //cerrar fichero
    }

    public static String llegirStrTeclat() {
        Scanner lector = new Scanner(System.in);
        String dades;
        dades = lector.nextLine();
        return dades;
    }

    public static int llegirIntTeclat() {
        Scanner lector = new Scanner(System.in);
        int enterLlegit = 0;
        boolean llegit = false;
        while (!llegit) {
            llegit = lector.hasNextInt();
            if (llegit) {
                enterLlegit = lector.nextInt();
            } else {
                System.out.println("ERROR DE TIPUS.");
                lector.next();
            }
        }
        lector.nextLine();
        return enterLlegit;
    }

    public static int llegirIntMinMax (int min, int max){
        int enterLlegit ;
        enterLlegit = llegirIntTeclat();
        while (enterLlegit < min || enterLlegit > max){
            System.out.println("ERROR DE RANG. MÍNIM: "+ min + ", MÀXIM: " + max);
            enterLlegit = llegirIntTeclat();
        }
        return enterLlegit;
    }

}

