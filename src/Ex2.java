import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class Ex2 {
    /*
    * 2.- Insertar dades en el fitxer "AleatorioEmple.dat". El mètode ha de rebre 4 paràmetres: identificador
    d'empleat, cognom, departament i salari, que li passem per paràmetre o bé introduïm per teclat. Abans d'insertar
    es comprovarà si l'identificador existeix, si existeix s'ha de visualitzar un missatge que ho indiqui, si no existeix
    s'ha d'insertar.
    */

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        File fichero = new File("AleatorioEmple.dat");
        //declara el fichero de acceso aleatorio
        RandomAccessFile file = new RandomAccessFile(fichero, "rw");

        System.out.println("Id de l'empleat");
        int id = llegirIntMinMax(1,10000);
        System.out.println("Cognom de l'empleat");
        String cognom = llegirStrTeclat();
        System.out.println("Departament");
        int dep = llegirIntTeclat();
        System.out.println("Salari");
        double salary = sc.nextDouble();
        if (!chechExist(fichero, id)){
            insertarRegistre(id, cognom,dep, salary);
        } else {
            System.out.println("El registre ja existeix!");
        }



    }

    public static void insertarRegistre(int id, String cognom, int dep, double salari){

    }

    public static boolean chechExist(File file, int id){
        int posicio = (id -1) * 36;
        if (posicio <= file.length())
        return true;
        else
            return false;
    }

    /*id = 0;
    cursor = 0;
    while (cursor <file.length)
    {
        file.seek(cursor)
        id = readInt()
        cursor += 36
    }
    sespera el id

    file.seek(file.length())
    file.write(id)
    cognomString = llegirStrTeclat();
    while(cognomString.length>10){
        System.out.println(Error supera 10 caracters);
    }
    file.writeBytes(cognomString)//mal! hay que escribir 10 caracteres
    departament = sc.nextInt()
    file.writeInt(departament)
    cognomString = llegirStrTeclat();

    sout(introdueix el salari)
    salary = sc.nextDouble();
    file.writeDouble(salari)*/


    public static String llegirStrTeclat() {
        Scanner lector = new Scanner(System.in);
        String dades;
        dades = lector.nextLine();
        return dades;
    }

    public static int llegirIntTeclat() {
        Scanner lector = new Scanner(System.in);
        int enterLlegit = 0;
        boolean llegit = false;
        while (!llegit) {
            llegit = lector.hasNextInt();
            if (llegit) {
                enterLlegit = lector.nextInt();
            } else {
                System.out.println("ERROR DE TIPUS.");
                lector.next();
            }
        }
        lector.nextLine();
        return enterLlegit;
    }

    public static int llegirIntMinMax (int min, int max){
        int enterLlegit ;
        enterLlegit = llegirIntTeclat();
        while (enterLlegit < min || enterLlegit > max){
            System.out.println("ERROR DE RANG. MÍNIM: "+ min + ", MÀXIM: " + max);
            enterLlegit = llegirIntTeclat();
        }
        return enterLlegit;
    }

}
